The ATLAS run database contains metadata about all "runs" executed in Point 1.
Stored information are: start and end timestamps, software partition.

`run_db` is a python module providing an API to query and search the ATLAS
run database. It can also be used as a standalone command line tool. The only
requirement is the TDAQ software release. Read the online help for a complete
list of features.

`WebRunSearch` is a web-based application interfacing `run_db`. It also binds
the search features of `run_db` to ATLAS Grafana. Two interfaces are actually
provided:
- A simple web page with a search box returning results from the database and
  generating links to all existing ATLAS Grafana dashboards. The links set
  the partition and the time boundaries for the selected run.
- A frame-base web page integrating the search feature with Grafana's interface
  directly. After selecting a dashboard, the user can search runs and navigate
  through time and partitions based on runs.

# run_db.py usage examples

- Source TDAQ release

  ```bash
  source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh [nightly]
  ```

- Check that you have access to EOS

  ```bash
  ll /eos/user/a/atdaqeos/adm/db
  ```

- Use run_db.py directly:

  ```bash
  ./run_db.py ongoing
  ...
  ./run_db.py 345678
  {345678, FTK_MultiBoards, 1520961092 (2018-03-13 18:11:32+01:00), 1520961...
  ./run_db.py date 20180507
  {349693, ATLAS, 1525709652 (2018-05-07 18:14:12+02:00), 1525754512 (2018-...
  {349692, ATLAS, 1525708055 (2018-05-07 17:47:3...
  ```

- Use as python module:

  ```python
  >>> import run_db
  >>> help(run_db)
  ...
  >>> run_db.search('345678')
  ('success', '', [{345678, 1520961092, 1520961111, FTK_MultiBoards}])
  ```

# WebRunSearch

## Deployment

- Same dependencies as `run_db`: a working TDAQ software release.
- `WebRunSearch` is based on `flask` for the web framework.
- A web server that supports WSGI is advised to run in a production environment.
  `webrunsearch.py` can nevertheless be used as a standalone application using
  `flask` internal WSGI server.

## Operations

Start and stop scripts are provided:
- to run the application with gunicorn (`start.gunicorn.bash`);
- to have supervisord run and monitor gunicorn (`start.bash`).

`supervisor.conf` contains the paths of the files you want to check if it does
not work:
- `/tmp/webrunsearch/gunicorn.log`
- `/tmp/webrunsearch/supervisord.log`

## Features

- http://server:9090
  - search the run database
  - display results in a table
  - generate list of links to Grafana's dashboards with time boundaries and
    partition variables preset for each run
- http://server:9090/pbeast
  - select a dashboard, then search a run: adjust time boundaries, partition and
    refresh variables for the current dashboard
  - not supported: changing dashboard loses current run parameters
- search box:
  - specific run number
  - "date 20180507": all runs started on this date
  - "last 5": last 5 runs
  - "previous 345678 5": 5 runs just before the given run number
  - ATLAS partition only: restrict searches to ATLAS partition only
