#!/usr/bin/env tdaq_python
import cx_Oracle
import datetime
import sys
import coral_auth

from dateutil import tz
UTC_TIMEZONE = tz.gettz('UTC')

class RunNumberDb:
    """
    Use ATLR DB to look for runs
    Returns (arrays of) RunInfo

    Usage:
        db = run_db.RunNumberDb()
        with db:
            run = db.get_by_run_number(323488)
    """

    def __init__(self, coral_url='oracle://atlr/rn_r'):
        self.url = coral_url

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        if exc_type is not None:
            raise exc_val

    def open(self):
        user, pwd, service_name = coral_auth.get_connection_parameters_from_connection_string(self.url)
        self.conn = cx_Oracle.connect(user, pwd, service_name)
        self.c = self.conn.cursor()

    def close(self):
        self.c.close()
        self.conn.close()

    def get_last_n(self, n, partition='ATLAS'):
        """
        Return the last n runs for the specified partition (default: ATLAS)
        """
        if (n is None) or (type(n) is not int) or (n <= 0):
            raise RuntimeError('wrong number of runs')

        if partition == '*':
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'order by STARTEDAT desc nulls last')
        else:
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where PARTITIONNAME=:1 '
                    'order by STARTEDAT desc nulls last', (partition,))

        return [self._convert(x) for x in self.c.fetchmany(n)]

    def get_by_run_number(self, rn):
        """
        Return the run with number==rn or raise RuntimeError
        """
        if rn == 'ongoing':
            runs = self.get_ongoing_run('ATLAS') # assuming ATLAS partition
            if runs:
                return runs[0]
            else:
                return []

        try:
            rn = int(rn)
        except (ValueError, TypeError):
            raise RuntimeError('run number cannot be converted to int: ' + str(rn))

        if (rn is None) or (type(rn) is not int) or (rn <= 0):
            raise RuntimeError('wrong run number: ' + str(rn))
        self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                       'from atlas_run_number.runnumber '
                       'where RUNNUMBER=:1 ', (rn,))
        return self._convert(self.c.fetchone())

    def get_previous_n(self, ref_rn, n, partition='ATLAS'):
        """
        Return the n runs just before ref_rn for the specified partition (default: ATLAS)
        """
        if (n is None) or (type(n) is not int) or (n <= 0):
            raise RuntimeError('wrong number of runs')

        try:
            ref_rn = int(ref_rn)
        except (ValueError, TypeError):
            raise RuntimeError('run number cannot be converted to int: ' + str(rn))

        if (ref_rn is None) or (type(ref_rn) is not int) or (ref_rn <= 0):
            raise RuntimeError('wrong reference run number: ' + str(ref_rn))

        if partition == '*':
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where RUNNUMBER<:1 '
                    'order by STARTEDAT desc nulls last', (ref_rn,))
        else:
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where PARTITIONNAME=:1 '
                    'and RUNNUMBER<:2 '
                    'order by STARTEDAT desc nulls last', (partition, ref_rn))

        return [self._convert(x) for x in self.c.fetchmany(n)]

    def get_ongoing_run(self, partition='ATLAS'):
        """
        Returns the latest run with a null duration for each partition.
        """
        if partition == '*':
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where DURATION IS NULL '
                    'order by STARTEDAT desc nulls last')
            res = []
            partitions = set()
            for row in self.c.fetchall():
                # sanity check: DB is full of incomplete entries
                if type(row[0]) is not int or \
                        type(row[1]) is not datetime.datetime or \
                        (row[2] is not None and type(row[2]) is not datetime.datetime) or \
                        type(row[3]) is not str:
                    continue
                if row[3] not in partitions:
                    partitions.add(row[3])
                    res.append(self._convert(row))
            return res
        else:
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where DURATION IS NULL '
                    'and STARTEDAT IS NOT NULL '
                    'and PARTITIONNAME=:1 '
                    'order by STARTEDAT nulls last'
                    , (partition,))
            run = self.c.fetchone()
            if run is None:
                return []
            else:
                return [self._convert(run)]

    def get_by_date(self, date, partition='ATLAS'):
        """
        Returns all run that started at the given date with format:
          YYYYMMDD
        """
        return self.get_by_dates(date, date, partition)

    def get_by_dates(self, startdate, enddate, partition='ATLAS'):
        """
        Returns runs that started between dates included in format: YYYYMMDD
        """
        # Convert to check format
        lower = datetime.datetime.strptime(startdate, '%Y%m%d')
        # next day midnight to include runs for the given date
        upper = datetime.datetime.strptime(enddate, '%Y%m%d') + datetime.timedelta(days=1)
        if partition == '*':
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where STARTEDAT>=:1 '
                    'and STARTEDAT<:2 '
                    'order by STARTEDAT', (lower, upper))
        else:
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where STARTEDAT>=:1 '
                    'and STARTEDAT<:2 '
                    'and PARTITIONNAME=:3 '
                    'order by STARTEDAT', (lower, upper, partition))

        return [self._convert(x) for x in self.c.fetchall()]

    def get_by_run_number_start(self, rnstart, partition='ATLAS'):
        re = '^' + str(rnstart)
        if partition == '*':
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where regexp_like(TO_CHAR(RUNNUMBER), :1)'
                    'order by STARTEDAT desc nulls last', (re,))
        else:
            self.c.execute('select RUNNUMBER,STARTEDAT,DURATION,PARTITIONNAME '
                    'from atlas_run_number.runnumber '
                    'where regexp_like(TO_CHAR(RUNNUMBER), :1) '
                    'and PARTITIONNAME=:2 '
                    'order by STARTEDAT desc nulls last', (re, partition))
        return [self._convert(x) for x in self.c.fetchall()]

    def _convert(self, raw):
        if not raw:
            raise RuntimeError('no such runs')
        if raw[1] is None:
            RuntimeError(f'old run format unsupported: {raw}')
        # STARTEDAT field is UTC
        sor = raw[1].replace(tzinfo=UTC_TIMEZONE).timestamp()
        if raw[2] is not None:
            eor = sor + raw[2]
        else:
            eor = None
        return RunInfo(raw[0], sor, eor, raw[3])


class RunInfo(object):
    """
    Information about a run: run number, start of run timestamp, end of run
    timestamp, partition.
    sor, eor: epoch timestamp in seconds (UTC)
    Localtime datetime through sor/eor_as_datetime()
    This class defines:
    - an equality operator
    - a string conversion operator
    - a repr() operator
    - a serialize() method that returns a dictionary representation of the
      object (to use for JSON serialization)
    """
    def __init__(self, run_number, sor, eor, partition):
        self.run_number = run_number
        self.sor = sor
        self.eor = eor
        self.partition = partition


    def __str__(self):
        return '{{{}, {}, {} ({}), {} ({})}}'.format(self.run_number,
                self.partition, self.sor,
                self.sor_as_datetime(), self.eor,
                self.eor_as_datetime() if self.eor is not None else '')


    def __repr__(self):
        return '{{{}, {}, {}, {}}}'.format(self.run_number,
                self.sor, self.eor, self.partition)


    def sor_as_datetime(self):
        # fromtimestamp produces a local time datetime
        return datetime.datetime.fromtimestamp(self.sor)


    def eor_as_datetime(self):
        # fromtimestamp produces a local time datetime
        if self.eor:
            return datetime.datetime.fromtimestamp(self.eor)
        else:
            return None

    def __eq__(self, rhs):
        return self.run_number == rhs.run_number \
                and self.sor == rhs.sor \
                and self.eor == rhs.eor \
                and self.partition == rhs.partition


    def serialize(self):
        return {
            'run_number': self.run_number,
            'sor': self.sor,
            'eor': self.eor,
            'partition': self.partition,
        }


def search(query):
    """
    Use ATLR database to look for run metadata: run number, start and stop
    timestamps, partition.
    Parameters:
        query: query string; see below for possible queries
        partition: partition string to look runs for; '*' for all partitions
    Returns: (status, message, [RunInfo...])
        status: 'success', 'error'
        message: '' on success, error message if status == 'error'
        [RunInfo...]: list of RunInfo for runs matching the query, empty if
                      status == 'error'. For consistency, even if is a single
                      run is fetched, a list is returned.

    Supported queries:
    - everywhere a partition can be provided:
      - a star (*) means all partitions
      - the parameter is optional; default is all partitions
    - "last {N:integer} [partition]": get the last N runs for the specified
      partition (or all partitions if unspecified)
    - "previous {reference:integer} {N:integer} [partition]": get the
      N runs directly before the reference run number
    - "ongoing [partition]": get the ongoing run for the given partition (or all
      partition if unspecified)
    - "date {date:string}": date in format "%Y%m%d" (ex: 20180507)
      Get all runs that started on this date for the specified partition (or all
      partitions if unspecified).
    - "{run number:integer}": get information for the given run number
    """
    status = 'success'
    message = ''
    runs = []

    if not query:
        status = 'error'
        message = 'empty query'
        return (status, message, runs)

    words = query.split()
    partition = '*' # default value

    with RunNumberDb() as rundb:
        try:
            if words[0] == 'last':
                if len(words) > 2:
                    partition = words[2]
                runs = rundb.get_last_n(int(words[1]), partition)
                return (status, message, runs)

            if words[0] == 'previous':
                if len(words) < 3:
                    status = 'error'
                    message = '"previous" command requires 2 parameters'
                    return (status, message, runs)
                if len(words) > 3:
                    partition = words[3]
                runs = rundb.get_previous_n(int(words[1]), int(words[2]), partition)
                return (status, message, runs)

            if words[0] == 'ongoing':
                if len(words) > 1:
                    partition = words[1]
                runs = rundb.get_ongoing_run(partition)
                if not runs:
                    status = 'warning'
                    message = 'no ongoing run'
                return (status, message, runs)

            if words[0] == 'date':
                if len(words) > 2:
                    partition = words[2]
                runs = rundb.get_by_date(words[1], partition)
                return (status, message, runs)

            if words[0] == 'dates':
                if len(words) > 3:
                    partition = words[3]
                runs = rundb.get_by_dates(words[1], words[2], partition)
                return (status, message, runs)

            runs = [rundb.get_by_run_number(int(words[0]))]
            return (status, message, runs)
        except Exception as e:
            status = 'error'
            message = str(e)

    return (status, message, runs)


def get(run_number):
    """
    Use ATLR DB to look for runs
    Returns (arrays of) RunInfo.
    Throws exception on errors: returned value is always valid.

    Supported input:
    - run number (integer) as int,
    - run number as string (containing int),
    - 'ongoing' special value.

    Usage:
        run_info = run_db.get(311365)
    """
    run = None
    with RunNumberDb() as rndb:
        if run_number == 'ongoing':
            run = rndb.get_ongoing_run()
        else:
            run = rndb.get_by_run_number(run_number)

    if run is None:
        raise RuntimeError('cannot get run #'+str(run_number))

    return run


def print_help():
    print(f'usage: {sys.argv[0]} {{query|--help|-h}}')
    print('  --help|-h: display help and exit')
    print('  query:')
    print('    "last {N:integer} [partition]": get the last N runs')
    print('    "previous {reference:integer} {N:integer} [partition]": get the N runs')
    print('      directly before the reference run number')
    print('    "ongoing [partition]": get the ongoing run')
    print('    "date {date:string in format %Y%m%d (e.g. 20180507)} [partition]"')
    print('      get all runs that started on this date')
    print('    "{run number:integer}": get information for the given run number')
    print('    "dates {startdate:string%Y%m%d} {enddate:string%Y%m%d}')
    print('    "  get all runs that started between startdate and enddate included')
    print('    partition: restrict search to the given partition if specified')


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print_help()
        sys.exit(1)
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help()
        sys.exit(0)
    status, errmsg, runs = search(' '.join(sys.argv[1:]))
    if status == 'success':
        for run in runs:
            print(run)
        sys.exit(0)
    else:
        print(f'{status}: {errmsg}')
        sys.exit(1)
