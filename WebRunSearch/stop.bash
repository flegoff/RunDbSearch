#!/usr/bin/env bash
LOGS_DIR='/tmp/webrunsearch'
TIMEOUT=30

ls $LOGS_DIR/supervisord.pid >/dev/null || exit 1
PID=$(cat $LOGS_DIR/supervisord.pid)
kill $PID || exit 2

n=0
while ps -p $PID &>/dev/null; do
    sleep 1
    n=$((n+1))
    if [ $n -ge 30 ]; then
        echo "timeout expired: force kill"
        kill -s 9 $PID
    fi
done

rm -f $LOGS_DIR/supervisord.pid
exit 0
