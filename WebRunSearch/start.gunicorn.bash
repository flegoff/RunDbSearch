#!/bin/bash
BASEDIR=$(realpath `dirname "$0"`)
LOGS_DIR='/tmp/webrunsearch'
PIDFILE="$LOGS_DIR/webrunsearch.pid"

if [ -f "$PIDFILE" ]; then
  echo "process already running: pid=`cat $PIDFILE`"
  exit -1
fi

mkdir -p "$LOGS_DIR"

source "$BASEDIR"/environment

# cannot access EOS when not logged in
# so copy the files we need from there and modify env accordingly
cp $CORAL_AUTH_PATH/authentication.xml $BASEDIR
cp $TNS_ADMIN/tnsnames.ora $BASEDIR
export CORAL_AUTH_PATH=$BASEDIR
export TNS_ADMIN=$BASEDIR

gunicorn -D -w 10 --chdir "$BASEDIR" \
  --pythonpath "$BASEDIR" -b :9090 webrunsearch:webrunsearch \
  --timeout 300 \
  --access-logfile "$LOGS_DIR"/access.log \
  --error-logfile "$LOGS_DIR"/error.log \
  --log-level info --pid "$PIDFILE" || echo "failed to start gunicorn"
