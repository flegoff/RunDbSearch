#!/bin/bash
BASEDIR=$(dirname "$0")
LOGS_DIR='/tmp/webrunsearch'

mkdir -p "$LOGS_DIR" || exit 1

source $BASEDIR/environment

cd $BASEDIR || exit 2
/usr/bin/supervisord -c ./supervisor.conf -i supervisor.webrunsearch || exit 3
exit 0

