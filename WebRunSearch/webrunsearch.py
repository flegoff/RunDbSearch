#!/usr/bin/env python
from flask import Flask
from flask import url_for, render_template, request, flash, abort, jsonify
import datetime
import json
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import run_db

webrunsearch = Flask(__name__)

@webrunsearch.route('/', methods=['GET'])
def default():
    return render_template('default.j2', page_title='pbeast dashboard')

@webrunsearch.route('/pbeast', methods=['GET'])
def pbeast():
    return render_template('pbeast.j2', page_title='pbeast dashboard')

@webrunsearch.route('/searchrun', methods=['GET'])
def searchrun():
    # new version
    query = request.args.get('query')
    atlasonly = request.args.get('atlasonly')
    if atlasonly == 'true':
        query += ' ATLAS'

    # Send runs in reverse order of appearance because javascript prepends
    # objects to container
    status, message, runs = run_db.search(query)
    return json.dumps({
            'status': status,
            'message': message.replace('\n', '<br/>'),
            'runs': [r.serialize() for r in runs[::-1]],
            });

@webrunsearch.route('/autocompleterunsearch', methods=['GET'])
def autocompleterunsearch():
    query = request.args.get('q')
    atlasonly = request.args.get('atlasonly')
    partition = 'ATLAS' if atlasonly == 'true' else '*'

    with run_db.RunNumberDb() as db:
        runs = [{
            'label': '{}: {}, {}'.format(str(run.run_number),
                    datetime.datetime.utcfromtimestamp(run.sor).strftime(
                    '%Y/%m/%d %H:%M:%S'),
                    run.partition),
            'value': str(run.run_number)
            } for run in db.get_by_run_number_start(query, partition=partition)]

    return json.dumps(runs)

if __name__ == '__main__':
    webrunsearch.run(host='0.0.0.0', port=9090)
