#!/usr/bin/env bash
LOGS_DIR='/tmp/webrunsearch'
kill $(cat $LOGS_DIR/webrunsearch.pid)
rm -f $LOGS_DIR/webrunsearch.pid
