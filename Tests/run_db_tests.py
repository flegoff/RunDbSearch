import unittest
import datetime
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import run_db

# Gitlab CI will run the tests.
# To do it manually:
# $ cm_setup nightly
# $ kinit # or whatever has to be done to be able to access $CORAL_AUTH_PATH
# $ python run_db_tests.py

class RunDbSearchTests(unittest.TestCase):

    def test_last(self):
        status, errmsg, runs = run_db.search('last')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('last truc')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('last 2')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 2)
        status, errmsg, runs = run_db.search('last 2 ATLAS')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 2)

    def test_previous(self):
        status, errmsg, runs = run_db.search('previous')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('previous truc')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('previous truc truc')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('previous truc 2')
        self.assertEqual(status, 'error')
        status, errmsg, runs = run_db.search('previous 438481 2')
        self.assertEqual(status, 'success')
        self.assertEqual(runs[0].run_number, 438480)
        self.assertEqual(runs[1].run_number, 438479)
        status, errmsg, runs = run_db.search('previous 438481 2 ATLAS')
        self.assertEqual(status, 'success')
        self.assertEqual(runs[0].run_number, 438464)
        self.assertEqual(runs[1].run_number, 438446)

    def test_ongoing(self):
        status, errmsg, runs = run_db.search('ongoing')
        self.assertTrue(status == 'success'
                or (status == 'warning' and errmsg == 'no ongoing run'))
        for r in runs:
            self.assertIsNone(r.eor)
        status, errmsg, runs = run_db.search('ongoing ATLAS')
        self.assertTrue(status == 'success'
                or (status == 'warning' and errmsg == 'no ongoing run'))
        for r in runs:
            self.assertIsNone(r.eor)

    def test_date(self):
        datestr = '20221101'
        date = datetime.datetime.strptime(datestr, '%Y%m%d')
        lower = date.timestamp()
        upper = (date + datetime.timedelta(days=1)).timestamp()

        status, errmsg, runs = run_db.search('date ' + datestr)
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 37)
        for r in runs:
            self.assertLessEqual(lower, r.sor)
            self.assertLess(r.sor, upper)

        status, errmsg, runs = run_db.search('date ' + datestr + ' ATLAS')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 3)
        for r in runs:
            self.assertLessEqual(lower, r.sor)
            self.assertLess(r.sor, upper)

    def test_dates(self):
        d1s = '20221031'
        d2s = '20221101'
        lower = datetime.datetime.strptime(d1s, '%Y%m%d').timestamp()
        upper = (datetime.datetime.strptime(d2s, '%Y%m%d')
                + datetime.timedelta(days=1)).timestamp()

        status, errmsg, runs = run_db.search(f'dates {d1s} {d2s}')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 118)
        for r in runs:
            self.assertLessEqual(lower, r.sor)
            self.assertLess(r.sor, upper)

        status, errmsg, runs = run_db.search(f'dates {d1s} {d2s} ATLAS')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 9)
        for r in runs:
            self.assertLessEqual(lower, r.sor)
            self.assertLess(r.sor, upper)

    def test_run_number(self):
        status, errmsg, runs = run_db.search('438481')
        self.assertEqual(status, 'success')
        self.assertEqual(len(runs), 1)
        self.assertEqual(runs[0].run_number, 438481)

    def test_errors(self):
        status, errmsg, runs = run_db.search('nope')
        self.assertEqual(status, 'error')

    def test_get(self):
        info = run_db.get(438481)
        self.assertEqual('{438481, ATLAS, 1667323235.118086 '
                '(2022-11-01 18:20:35.118086), 1667380976.118086 '
                '(2022-11-02 10:22:56.118086)}', str(info))

        info = run_db.get('438481')
        self.assertEqual('{438481, ATLAS, 1667323235.118086 '
                '(2022-11-01 18:20:35.118086), 1667380976.118086 '
                '(2022-11-02 10:22:56.118086)}', str(info))

        infos = run_db.get('ongoing')
        if len(infos) == 1:
            self.assertTrue(isinstance(infos[0], run_db.RunInfo))
            self.assertEqual(infos[0].eor, None)
            self.assertEqual(infos[0].partition, 'ATLAS')
        elif len(infos) == 0:
            pass
        else:
            self.assertTrue(False, msg="ongoing returned more than 1 run")


if __name__ == '__main__':
    unittest.main()
